package com.example.demo.client;

import java.io.File;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AWSS3FileUploadDemo implements CommandLineRunner {
	

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Hello world From Spring Boot 2 !!");
		System.out.println("Uploading file to S3 bucket :: ");
		
		AWSCredentials awsCredentials = 
				new BasicAWSCredentials("AKIA4DNDJF4CPKZXBMIR", "9R3z8g+vPPyQAyI4qI/JGuhm1BNcCxDH/NVks2RN");
		
		AmazonS3 amazonS3 = AmazonS3ClientBuilder
			.standard()
			.withRegion("ap-south-1")
			.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
			.build();
		
		File file = new File("D://data//test.txt");
		
		amazonS3.putObject("classpathio-test-bucket", "file-upload", file );
		
		
	}

}
