package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsDemosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsDemosApplication.class, args);
	}

}
